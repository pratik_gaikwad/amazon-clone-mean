const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const config = require('./config')
const jwt = require("jsonwebtoken");
// swagger: for api documentation
const swaggerJSDoc = require("swagger-jsdoc");
const swaggerUiExpress = require('swagger-ui-express')


const app = express();
app.use(bodyParser.json());


// morgan 
app.use(morgan("combined"));

// routers 
const adminRouter = require("./admin/routes/admin");
const brandRouter = require('./admin/routes/brand')
const categoryRouter = require('./admin/routes/category')
const orderRouter = require('./admin/routes/order')
const productRouter = require('./admin/routes/product')
const reviewRouter = require('./admin/routes/review')

// swagger init
const swaggerDefinition = {
  info: {
    title: 'Amazon Server',
    version: '1.0.0',
    description: 'This is a Express server for amazon application'
  },
  basePath: '/'
}

const swaggerOptions = {
  swaggerDefinition,
  apis: ['./admin/routes/*.js']
}

const swaggerSpec = swaggerJSDoc(swaggerOptions)
app.use(
  "/admin-api-docs",
  swaggerUiExpress.serve,
  swaggerUiExpress.setup(swaggerSpec)
);

// add a middleware for getting the id from token
function getUserId(request, response, next) {

  if (
    request.url == "/admin/signin" ||
    request.url == "/admin/signup" ||
    request.url == "/iphone.jpg"
  ) {
    // do not check for token
    next();
  } else {
    try {
      const token = request.headers["token"];
      // console.log(token)
      const data = jwt.verify(token, config.secret);
      // console.log(data)

      // add a new key named userId with logged in user's id
      request.userId = data["id"];

      // go to the actual route
      next();
    } catch (ex) {
      response.status(401);
      response.send({ status: "error", error: "protected api" });
    }
  }
}

app.use(getUserId)

// required to send the static images
app.use(express.static('images/'))


// add the routes
app.use("/admin", adminRouter);
app.use('/brand', brandRouter)
app.use('/category', categoryRouter)
app.use('/order', orderRouter)
app.use('/product', productRouter)
app.use('/review', reviewRouter)


// default route
app.get('/', (request, response) => {
  response.send('welcome to amazon')
})

app.listen(3000, '0.0.0.0', () => {
  console.log('server started on port 3000')
})
