const express = require("express");
const db = require("../../db");
const config = require("../../config");
const utils = require("../../utils");
const { request, response } = require("express");
const mailer = require("../../mailer");
const crypto = require("crypto-js");
const uuid = require("uuid");
const fs = require("fs");
const path = require("path");
const router = express.Router();
const jwt = require("jsonwebtoken");
const { dir } = require("console");

// ---------------------------------------
//                  GET
// ---------------------------------------

router.get("/activate/:token", (request, response) => {
  const { token } = request.params;
  console.log(token);
  // reset the token & activate user
  const statement = `UPDATE user SET active=1,activationToken='' WHERE activationToken='${token}' `;

  db.query(statement, (error, data) => {
    let htmlPath = path.join(__dirname, "/../templates/activation_result.html");
    let body = "" + fs.readFileSync(htmlPath);
    response.header("Content-Type", "text/html");

    response.send(body);
  });
});

// forgort password router

router.get("/forgot-password/:email", (request, response) => {
  const { email } = request.params;
  const statement = `select id, firstName, lastName from user where email = '${email}'`;
  db.query(statement, (error, users) => {
    if (error) {
      response.send(utils.createError(error));
    } else if (users.length == 0) {
      console.log(`type of users is ${typeof users}`);
      console.log(users.lenght);

      response.send(utils.createError("user does not exist"));
    } else {
      console.log(users.lenght);
      console.log(`type of users is ${typeof users}`);
      const user = users[0];
      const otp = utils.generateOTP();
      let htmlPath = path.join(__dirname, "/../templates/forgotPassword.html");

      let body = "" + fs.readFileSync(htmlPath);
      body = body.replace("email", email);
      body = body.replace("otp", otp);

      mailer.sendEmail(email, "reset your Password", body, (error, info) => {
        console.log(error);
        console.log(info);
        response.send(
          utils.createResult(error, {
            email: email,
            OTP: otp,
          })
        );
      });
    }
  });
});

// reset password
router.get("/reset-password/:password", (request, response) => {
  const { password } = request.params;
  console.log(password)
  const { userId } = `${request["userId"]}`;
  const encryptedPassword = crypto.SHA256(password);
  console.log(`${encryptedPassword}`)
  console.log(`${userId}`);

  const statement = `UPDATE user SET password='${encryptedPassword}' where id = ${request["userId"]} `;
  db.query(statement, (error, data) => {
    if (error) {
      response.send(utils.createError(error));
    } else {
      response.send(utils.createResult(error,"password changed successfully"));
    }
  });
});

// ---------------------------------------
//                  POST

/**
 * @swagger
 *
 * /user/signup:
 *   post:
 *     description: user sign up
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: firstName
 *         description: First Name
 *         in: formData
 *         required: true
 *         type: string
 *       - name: lastName
 *         description: First Name
 *         in: formData
 *         required: true
 *         type: string
 *       - name: email
 *         description: user email
 *         in: formData
 *         required: true
 *         type: string
 *       - name: password
 *         description: user password
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: successful message
 */
// router.post("/signup", (req, res) => {
//   // Your implementation comes here ...
//     console.log("inside signup func");

// });

router.post("/signup", (request, response) => {
  const { firstName, lastName, email, password } = request.body;

  const activationToken = uuid.v4();
  const activationLink = `http://localhost:4000/user/activate/${activationToken}`;

  const htmlPath = path.join(
    __dirname,
    "/../templates/send_activation_link.html"
  );
  let body = "" + fs.readFileSync(htmlPath);
  body = body.replace("firstName", firstName);
  body = body.replace("activationLink", activationLink);

  const statement = `insert into user (firstName, lastName, email, password, activationToken) values (
    '${firstName}', '${lastName}', '${email}', '${crypto.SHA256(
    password
  )}', '${activationToken}') `;
  db.query(statement, (error, data) => {
    mailer.sendEmail(email, "Welcome to amazon-clone", body, (error, info) => {
      console.log(error);
      console.log(info);
      response.send(utils.createResult(error, data));
    });
  });
});

router.post("/signin", (request, response) => {
  const { email, password } = request.body;
  const statement = `select id, firstName, lastName, active from user where email = '${email}' and password = '${crypto.SHA256(
    password
  )}'`;
  db.query(statement, (error, users) => {
    if (error) {
      response.send({ status: "error", error: error });
    } else if (users.length == 0) {
      response.send({ status: "error", error: "user does not exist" });
    } else {
      const user = users[0];
      if (user["active"] == 1) {
        // user is an active user
        const token = jwt.sign({ id: user["id"] }, config.secret);
        response.send(
          utils.createResult(error, {
            firstName: user["firstName"],
            lastName: user["lastName"],
            token: token,
          })
        );
      } else {
        // user is a suspended user
        response.send({
          status: "error",
          error: "your account is not active. please contact administrator",
        });
      }
    }
  });
});

// ---------------------------------------

// ---------------------------------------
//                  PUT
// ---------------------------------------

// ---------------------------------------
//                  DELETE
// ---------------------------------------

module.exports = router;
