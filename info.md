```javascript

**skeleton of application**
    admin-server
          -server.js            <-----express connection
          -utils.js             <-----utilities(createResult() func)
          -db.js                <-----for mysql/mysql2 db connection/pool

    -ROUTES                <-----routes for creaing router
        -admin.js
        -product.js
        -brand.js
        -category.js
        - order.js
        -review.js

user-server
          -user-server.js            <-----express connection
    -ROUTES                <-----routes for creaing router
        -user.js
      - product management
      - wishlist
      - cart managment
      - order managment
      - profile management
      - address management


**importtant module in nodejs**
    -express
    -mysql2
    -crypto-js             <-------use to encryption
    -jsonwebtoken
    -morgon                <-------use to show log
    -mailer
    -multer
    -swagger jsdoc & ui-express

```

**Documentation swagger packages manager**

- swagger-jsdoc@4.0.0
  -npm install swagger-jsdoc --save

- swagger-ui-express@4.1.4
  -npm install swagger-ui-express --save

```nodejs
const swaggerJSDoc = require('swagger-jsdoc');

const options = {
  definition: {
    openapi: '3.0.0', // Specification (optional, defaults to swagger: '2.0')
    info: {
      title: 'Hello World', // Title (required)
      version: '1.0.0', // Version (required)
    },
  },
  // Path to the API docs
  apis: ['./routes.js'],
};

// Initialize swagger-jsdoc -> returns validated swagger spec in json format
const swaggerSpec = swaggerJSDoc(options);

```

```nodejs
How to document the API
The API can be documented in JSDoc-style with swagger spec in YAML.

/**
 * @swagger
 *
 * /login:
 *   post:
 *     description: Login to the application
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: username
 *         description: Username to use for login.
 *         in: formData
 *         required: true
 *         type: string
 *       - name: password
 *         description: User's password.
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: login
 */
app.post('/login', (req, res) => {
  // Your implementation comes here ...
});
```

**multer-middleware to upload file**
Multer is a node.js middleware for handling multipart/form-data, which is primarily used for uploading files. It is written on top of busboy for maximum efficiency.
```nodejs
var express = require('express')
var multer = require('multer')
var upload = multer({ dest: 'uploads/' })

var app = express()

app.post('/profile', upload.single('avatar'), function (req, res, next) {
// req.file is the `avatar` file
// req.body will hold the text fields, if there were any
})

app.post('/photos/upload', upload.array('photos', 12), function (req, res, next) {
// req.files is array of `photos` files
// req.body will contain the text fields, if there were any
})

var cpUpload = upload.fields([{ name: 'avatar', maxCount: 1 }, { name: 'gallery', maxCount: 8 }])
app.post('/cool-profile', cpUpload, function (req, res, next) {
// req.files is an object (String -> Array) where fieldname is the key, and the value is array of files
//
// e.g.
// req.files['avatar'][0] -> File
// req.files['gallery'] -> Array
//
// req.body will contain the text fields, if there were any
})
```
