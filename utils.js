function createResult(error, data) {
  return error ? createError(error) : createSuccess(data);
}

function createSuccess(data) {
  const result = {};
  result["status"] = "success";
  result["data"] = data;

  return result;
}

function createError(error) {
  const result = {};
  result["status"] = "error";
  result["error"] = error;

  return result;
}

function randomNumbers(min, max) {
  return Math.floor(Math.random() * (max - min) + max);
}

function generateOTP() {
  const otp = randomNumbers(10000, 99999);
  return otp;
}

module.exports = {
  createResult: createResult,
  createError: createError,
  createSuccess: createSuccess,
  generateOTP: generateOTP,
};
