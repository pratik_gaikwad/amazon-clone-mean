const express = require("express");
const utils = require("../../utils");
const db = require("../../db");
const config = require("../../config");
const { request } = require("express");
const router = express.Router();

// ----------------------------------------------------
// GET
// ----------------------------------------------------

/**
 * @swagger
 *
 * /review/product-review :
 *   get:
 *     description: get the product review 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: id of product
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: successful message
 */
router.get("/:productId", (req, res) => {
  // Your implementation comes here ...
});


router.get("/:productId", (request, response) => {
  const { productId } = request.params;
  const statement = `select id, userId, review, rating from productReviews where productId = ${productId}`;
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data));
  });
});

// ----------------------------------------------------

// ----------------------------------------------------
// POST
// ----------------------------------------------------

// manage review for admin not to add any review
// router.post("/:productId", (request, response) => {
//   const { productId } = request.params;
//   const { review, rating } = request.body;

//   const statement = `insert into productReviews (review, productId, userId, rating) values (
//     '${review}', '${productId}', '${request.userId}', '${rating}'
//   )`;
//   db.query(statement, (error, data) => {
//     response.send(utils.createResult(error, data));
//   });
// });

// ----------------------------------------------------

// ----------------------------------------------------
// DELETE
// ----------------------------------------------------

/**
 * @swagger
 *
 * /review/delete :
 *   delete:
 *     description: delete the product review 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: id of product
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: successful message
 */
router.delete("/:id", (req, res) => {
  // Your implementation comes here ...
});


router.delete("/:id", (request, response) => {
  const { id } = request.params;
  const statement = `delete from productReviews where id = ${id}`;
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data));
  });
});
// ----------------------------------------------------

module.exports = router;
