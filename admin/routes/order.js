const express = require('express')
const utils = require('../../utils')
const db = require('../../db')
const config = require('../../config')
const { request } = require('express')
const router = express.Router()

// ----------------------------------------------------
// GET
// ----------------------------------------------------

router.get('/allOrders', (request, response) => {
  const statement=`SELECT * FROM orderDetails`
  db.query(statement,(error,result)=>{
    response.send( utils.createResult(error,result) );
  })
  
})

// ----------------------------------------------------


// ----------------------------------------------------
// POST
// ----------------------------------------------------

router.post("/", (request, response) => {
  const { title, description, category, price, brand } = request.body;
  const statement = `insert into product (title, description, category, price, brand) values (
    '${title}', '${description}', '${category}', '${price}', '${brand}'
  )`;
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data));
  });
});

// ----------------------------------------------------


// ----------------------------------------------------
// PUT
// ----------------------------------------------------

// - update order
//         - update delivery status
//         - update products

router.put('/update/delivery-status/:userId', (request, response) => {
const {userId}=request.params
const {deliveryStatus}=request.body
const statement=`UPDATE userOrder SET deliveryStatus='${deliveryStatus}' 
                WHERE userId='${userId}'`
db.query(statement,(error,result)=>{
  response.send( utils.createResult(error,result) )
})                
})

// ----------------------------------------------------



// ----------------------------------------------------
// DELETE
// ----------------------------------------------------

router.delete('/delete/:userId', (request, response) => {
  response.send()
})

// ----------------------------------------------------

module.exports = router