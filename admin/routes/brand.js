const express = require("express");
const utils = require("../../utils");
const db = require("../../db");
const config = require("../../config");
const { request } = require("express");
const { createResult } = require("../../utils");
const router = express.Router();

// ----------------------------------------------------
// GET
// ----------------------------------------------------

/**
 * @swagger
 *
 * /All-brand:
 *   get:
 *     description: brand details
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: login
 */
// router.get("/", (req, res) => {
//   // Your implementation comes here ...

// });

router.get("/", (request, response) => {
  const { title, description } = request.body;
  const statement = `SELECT title,description FROM brand`;
  db.query(statement, (error, brands) => {
    response.send(utils.createResult(error, brands));
  });
});

// ----------------------------------------------------

// ----------------------------------------------------
// POST
// ----------------------------------------------------

/**
 * @swagger
 *
 * /brand/add-new-brand:
 *   post:
 *     description: add brand detail's
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: title
 *         description: title of brand
 *         in: formData
 *         required: true
 *         type: string
 *       - name: description
 *         description: description of brand
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: successful message
 */
// router.post("/add-new-brand", (req, res) => {
//   // Your implementation comes here ...
// });

router.post("/add-new-brand", (request, response) => {
  const { title, description } = request.body;
  const statement = `insert into brand (title, description) values ('${title}', '${description}') `;
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data));
  });
});

// ----------------------------------------------------

// ----------------------------------------------------
// PUT
// ----------------------------------------------------

/**
 * @swagger
 *
 * /brand/update:
 *   put:
 *     description: update brand detail's by brand id
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: id of brand
 *         in: formData
 *         required: true
 *         type: string
 *       - name: title
 *         description: title of brand
 *         in: formData
 *         required: true
 *         type: string
 *       - name: description
 *         description: description of brand
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: successful message
 */
// router.put("/update/:id", (req, res) => {
//   // Your implementation comes here ...
// });

router.put("/:id", (request, response) => {
  const { id } = request.params;
  const { title, description } = request.body;
  const statement = `update brand set title = '${title}', description = '${description}' where id = ${id}`;
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data));
  });
});

// ----------------------------------------------------

// ----------------------------------------------------
// DELETE
// ----------------------------------------------------

/**
 * @swagger
 *
 * /brand/delete:
 *   delete:
 *     description: delete brand by brand id
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: id of brand
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: successful message
 */
// router.delete("/delete/:id", (req, res) => {
//   // Your implementation comes here ...
// });

router.delete("/delete/:id", (request, response) => {
  const { id } = request.params;
  const statement = `delete from brand where id = ${id}`;
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data));
  });
});

// ----------------------------------------------------

module.exports = router;
