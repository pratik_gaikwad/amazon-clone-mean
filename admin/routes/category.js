const express = require("express");
const utils = require("../../utils");
const db = require("../../db");
const router = express.Router();

// ----------------------------------------------------
// GET
// ----------------------------------------------------

/**
 * @swagger
 *
 * /All-categories:
 *   get:
 *     description: get all catogories
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: login
 */
router.get("/", (req, res) => {
  // Your implementation comes here ...
});

router.get("/", (request, response) => {
  const statement = `select id, title, description from category`;
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data));
  });
});

// ----------------------------------------------------

// ----------------------------------------------------
// POST
// ----------------------------------------------------

/**
 * @swagger
 *
 * /category/add-new-category:
 *   post:
 *     description: add category detail's
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: title
 *         description: title of category
 *         in: formData
 *         required: true
 *         type: string
 *       - name: description
 *         description: description of category
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: successful message
 */
router.post("/create", (req, res) => {
  // Your implementation comes here ...
});

router.post("/create", (request, response) => {
  const { title, description } = request.body;
  const statement = `insert into category (title, description) values ('${title}', '${description}')`;
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data));
  });
});

// ----------------------------------------------------

// ----------------------------------------------------
// PUT
// ----------------------------------------------------

/**
 * @swagger
 *
 * /category/update:
 *   put:
 *     description: update category detail's
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: id of category
 *         in: formData
 *         required: true
 *         type: string
 *       - name: title
 *         description: title of category
 *         in: formData
 *         required: true
 *         type: string
 *       - name: description
 *         description: description of category
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: successful message
 */
router.put("/:id", (req, res) => {
  // Your implementation comes here ...
});


router.put("/:id", (request, response) => {
  const { id } = request.params;
  const { title, description } = request.body;
  const statement = `update category set title = '${title}', description = '${description}' where id = ${id}`;
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data));
  });
});

// ----------------------------------------------------

// ----------------------------------------------------
// DELETE
// ----------------------------------------------------

/**
 * @swagger
 *
 * /category/delete:
 *   delete:
 *     description: delete category
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: id of category
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: successful message
 */
router.delete("/:id", (req, res) => {
  // Your implementation comes here ...
});


router.delete("/:id", (request, response) => {
  const { id } = request.params;
  const statement = `delete from category where id = ${id}`;
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data));
  });
});

// ----------------------------------------------------

module.exports = router;
