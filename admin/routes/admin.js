const express = require("express");
const utils = require("../../utils");
const db = require("../../db");
const config = require("../../config");
const { request } = require("express");
const router = express.Router();
const crypto = require("crypto-js");
const jwt = require("jsonwebtoken");

// ----------------------------------------------------
// GET
// ----------------------------------------------------

/**
 * @swagger
 *
 * /Admin-profile:
 *   get:
 *     description: get user profile
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: login
 */
// router.get("/profile", (req, res) => {
//   // Your implementation comes here ...
// });

router.get("/profile", (request, response) => {
  const statement = `select firstName, lastName, email, phone from admin where id = ${request["userId"]}`;
  db.query(statement, (error, users) => {
    if (error) {
      response.send({ status: "error", error: error });
    } else {
      if (users.length == 0) {
        response.send({ status: "error", error: "user does not exist" });
      } else {
        // console.log(users)
        const user = users[0];
        response.send(utils.createResult(error, user));
      }
    }
  });
});

// ----------------------------------------------------

// ----------------------------------------------------
// POST
// ----------------------------------------------------

/**
 * @swagger
 *
 * /admin/signup:
 *   post:
 *     description: For signing up an administrator
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: firstName
 *         description: first name of admin user
 *         in: formData
 *         required: true
 *         type: string
 *       - name: lastName
 *         description: last name of admin user
 *         in: formData
 *         required: true
 *         type: string
 *       - name: email
 *         description: email of admin user used for authentication
 *         in: formData
 *         required: true
 *         type: string
 *       - name: password
 *         description: admin's password.
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: successful message
 */
router.post("/admin/signup", (req, res) => {
  // Your implementation comes here ...
});

router.post("/signup", (request, response) => {
  const { firstName, lastName, email, password } = request.body;
  const encryptedPassword = crypto.SHA256(password);

  const statement = `insert into admin (firstName, lastName, email, password) values (
        '${firstName}', '${lastName}', '${email}', '${encryptedPassword}'
      )`;
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data));
  });
});

/**
 * @swagger
 *
 * /admin/signin:
 *   post:
 *     description: Login to the application
 *     produces:
 *       - application/json
 *     parameters:
 *       - email: email
 *         description: email of admin for login.
 *         in: formData
 *         required: true
 *         type: string
 *       - name: password
 *         description: admin's password.
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: login
 */
router.post("/admin/signin", (req, res) => {
  // Your implementation comes here ...
});

router.post("/signin", (request, response) => {
  const { email, password } = request.body;
  const statement = `select id, firstName, lastName from admin where email = '${email}' and password = '${crypto.SHA256(
    password
  )}'`;
  db.query(statement, (error, admins) => {
    if (error) {
      response.send({ status: "error", error: error });
    } else {
      if (admins.length == 0) {
        response.send({ status: "error", error: "user does not exist" });
      } else {
        const admin = admins[0];
        const token = jwt.sign({ id: admin["id"] }, "1234567890");
        response.send(
          utils.createResult(error, {
            firstName: admin["firstName"],
            lastName: admin["lastName"],
            token: token,
          })
        );
      }
    }
  });
});

// ----------------------------------------------------

// ----------------------------------------------------
// PUT
// ----------------------------------------------------

router.put("", (request, response) => {
  response.send();
});

// ----------------------------------------------------

// ----------------------------------------------------
// DELETE
// ----------------------------------------------------

router.delete("", (request, response) => {
  response.send();
});

// ----------------------------------------------------

module.exports = router;
