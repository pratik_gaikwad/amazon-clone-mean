const express = require("express");
const utils = require("../../utils");
const db = require("../../db");
const router = express.Router();

// multer for uploading document jpg mp4 mp3
const multer = require("multer");
const upload = multer({ dest: "images/" });

// ----------------------------------------------------
// GET
// ----------------------------------------------------
/**
 * @swagger
 *
 * /products:
 *   get:
 *     description: get all catogories
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: login
 */
// router.get("/", (req, res) => {
//   // Your implementation comes here ...
// });

router.get("/", (request, response) => {
  const statement = `
      select p.id, p.title, p.description, 
        c.id as categoryId, c.title as categoryTitle,
        b.id as brandId, b.title as brandTitle,
        p.price, p.image, p.isActive from product p
      inner join category c on c.id = p.category
      inner join brand b on b.id = p.brand
      where p.isActive = 1

  `;
  db.query(statement, (error, data) => {
    if (error) {
      response.send(utils.createError(error));
    } else {
      // empty products collection
      const products = [];

      // iterate over the collection and modify the structure
      for (let index = 0; index < data.length; index++) {
        const tmpProduct = data[index];
        const product = {
          id: tmpProduct["id"],
          title: tmpProduct["title"],
          description: tmpProduct["description"],
          price: tmpProduct["price"],
          isActive: tmpProduct["isActive"],
          brand: {
            id: tmpProduct["brandId"],
            title: tmpProduct["brandTitle"],
          },
          category: {
            id: tmpProduct["categoryId"],
            title: tmpProduct["categoryTitle"],
          },
        };
        products.push(product);
      }

      response.send(utils.createSuccess(products));
    }
  });
});

// ----------------------------------------------------

// ----------------------------------------------------
// POST
// ----------------------------------------------------

// add product image by product id
/**
 * @swagger
 *
 * /product/add-image:
 *   post:
 *     description: add product image
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: id of product
 *         in: formData
 *         required: true
 *         type: string
 *       - name: image
 *         description: image of product
 *         in: formData
 *         required: true
 *         type: file
 *     responses:
 *       200:
 *         description: successful message
 */
// router.post("/upload-image/:productId", (req, res) => {
//   // Your implementation comes here ...
// });

router.post(
  "/upload-image/:productId",
  upload.single("image"),
  (request, response) => {
    const { productId } = request.params;
    const fileName = request.file.filename;

    const statement = `update product set image = '${fileName}' where id = ${productId}`;
    db.query(statement, (error, data) => {
      response.send(utils.createResult(error, data));
    });
  }
);

// add new product

/**
 * @swagger
 *
 * /product/add-new-product:
 *   post:
 *     description: add product detail's
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: title
 *         description: title of product
 *         in: formData
 *         required: true
 *         type: string
 *       - name: description
 *         description: description of product
 *         in: formData
 *         required: true
 *         type: string
 *       - name: category
 *         description: category of product
 *         in: formData
 *         required: true
 *         type: string
 *       - name: price
 *         description: price of product
 *         in: formData
 *         required: true
 *         type: string
 *       - name: brand
 *         description: brand of product
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: successful message
 */
// router.post("/create", (req, res) => {
//   // Your implementation comes here ...
// });

router.post("/create", (request, response) => {
  const { title, description, category, price, brand } = request.body;
  const statement = `insert into product (title, description, category, price, brand) values (
    '${title}', '${description}', '${category}', '${price}', '${brand}'
  )`;
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data));
  });
});

// ----------------------------------------------------

// ----------------------------------------------------
// PUT
// ----------------------------------------------------

/**
 * @swagger
 *
 * /product/update:
 *   put:
 *     description: update product detail's
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: id of product
 *         in: formData
 *         required: true
 *         type: string
 *       - name: title
 *         description: title of product
 *         in: formData
 *         required: true
 *         type: string
 *       - name: description
 *         description: description of product
 *         in: formData
 *         required: true
 *         type: string
 *       - name: price
 *         description: price of product
 *         in: formData
 *         required: true
 *         type: number
 *     responses:
 *       200:
 *         description: successful message
 */
// router.put("/:id", (req, res) => {
//   // Your implementation comes here ...
// });

router.put("/:id", (request, response) => {
  const { id } = request.params;
  const { title, description, price } = request.body;
  const statement = `UPDATE product 
                     SET title='${title}',description='${description}',price=${price} 
                     WHERE id=${id}  `;
  db.query(statement, (error, product) => {
    if (error) {
      response.send(utils.createError(error));
    } else {
      response.send(utils.createSuccess(product));
    }
  });
});

// change product state active/available(1) or not(0)  -----product availability

/**
 * @swagger
 *
 * /product/update/available-Or-Not:
 *   put:
 *     description: product availability
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: id of product
 *         in: formData
 *         required: true
 *         type: string
 *       - name: isActive
 *         description: product available(1) or Not(0)
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: successful message
 */
// router.put("/update-state/:id/:isActive", (req, res) => {
//   // Your implementation comes here ...
// });

router.put("/update-state/:id/:isActive", (request, response) => {
  const { id, isActive } = request.params;
  const statement = `update product set 
      isActive = ${isActive}
    where id = ${id}`;
  db.query(statement, (error, data) => {
    response.send(utils.createResult(error, data));
  });
});

// ----------------------------------------------------

// ----------------------------------------------------
// DELETE
// ----------------------------------------------------

/**
 * @swagger
 *
 * /product/delete:
 *   delete:
 *     description: delete product
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: enter id of product to be delete
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: successful message
 */
// router.delete("/:id", (req, res) => {
//   // Your implementation comes here ...
// });

router.delete("/:id", (request, response) => {
  const { id } = request.params;
  const statement = `DELETE FROM product WHERE id=${id}`;
  db.query(statement, (error, result) => {
    if (error) {
      response.send(utils.createError(error));
    } else {
      response.send(utils.createSuccess(result));
    }
  });
});

// ----------------------------------------------------

module.exports = router;
